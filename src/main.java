public class main {
    public static void main(String args[]){
        String example1 = "123-647-EYES";
        String example2 = "(325)444-TEST";
        String example3 = "653-879-8447";
        String example4 = "435-224-7613";

        System.out.println(ScanString(example1));
        System.out.println(ScanString(example2));
        System.out.println(ScanString(example3));
        System.out.println(ScanString(example4));
        /*System.out.println(ConvertPhoneNumberWithReplaceAll(example1));
        System.out.println(ConvertPhoneNumberWithReplaceAll(example2));
        System.out.println(ConvertPhoneNumberWithReplaceAll(example3));
        System.out.println(ConvertPhoneNumberWithReplaceAll(example4));
        */
    }
    public static String ScanString(String input){
        StringBuilder NumericPhonenumber = new StringBuilder();
        for(int i = 0; i < input.length(); i++){
            NumericPhonenumber.append(ConvertCharFromLetterToNumber(input.charAt(i)));
        }
        return NumericPhonenumber.toString();
    }

    public static char ConvertCharFromLetterToNumber(char input){
        char output = input;
        if (!Character.isDigit(input)) {
            switch (input) {
                case 'A':
                case 'B':
                case 'C':
                    output = '2';
                    break;
                case 'D':
                case 'E':
                case 'F':
                    output = '3';
                    break;
                case 'G':
                case 'H':
                case 'I':
                    output = '4';
                    break;
                case 'J':
                case 'K':
                case 'L':
                    output = '5';
                    break;
                case 'M':
                case 'N':
                case 'O':
                    output = '6';
                    break;
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                    output = '7';
                    break;
                case 'T':
                case 'U':
                case 'V':
                    output = '8';
                    break;
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    output = '9';
                    break;
            }
        }
        return output;
    }

    public static String ConvertPhoneNumberWithReplaceAll(String input){
        input = input.toLowerCase();
        input = input.replaceAll("[a-c]", "2");
        input = input.replaceAll("[d-f]","3");
        input = input.replaceAll("[g-i]", "4");
        input = input.replaceAll("[j-l]","5");
        input = input.replaceAll("[m-o]", "6");
        input = input.replaceAll("[p-s]","7");
        input = input.replaceAll("[t-v]", "8");
        input = input.replaceAll("[w-z]","9");
        return input;
    }
}
